local sensorInfo = {
	name = "Definition",
	desc = "Return location of a unit as Vec3",
	author = "Martin Dvorak",
	date = "2017-05-31",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1 -- instant, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

return function(unitID)
	local x, y, z = Spring.GetUnitPosition(unitID)
	return Vec3(x, y, z)
end