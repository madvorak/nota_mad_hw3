function getInfo()
	return {
		onNoUnits = SUCCESS,
		tooltip = "Try to unload one unit.",
		parameterDefs = {
			{ 
				name = "atlasID",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
			{ 
				name = "x",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
			{ 
				name = "z",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
		}
	}
end

-- constants
local THRESHOLD_DEFAULT = 0

local function ClearState(self)
	self.threshold = THRESHOLD_DEFAULT
end

function Run(self, units, parameter)
	local who = parameter.atlasID -- unitID
	local x = parameter.x
	local z = parameter.z
	
	if (who == nil) then
		return FAILURE
	end
	
	if (#Spring.GetUnitIsTransporting(who) > 0) then 
		Spring.GiveOrderToUnit(who, CMD.UNLOAD_UNITS, {x, 0, z, 20}, {})
		return RUNNING		
	else
		return SUCCESS
	end
end


function Reset(self)
	ClearState(self)
end
