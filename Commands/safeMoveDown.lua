function getInfo()
	return {
		onNoUnits = SUCCESS,
		tooltip = "Move safely down",
		parameterDefs = {
			{ 
				name = "unitID",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			}
		}
	}
end

local function ClearState(self)
	self.threshold = THRESHOLD_DEFAULT
end

local function shouldRun(unitID)
	if (global.unitsRunningDown == nil) then
		global.unitsRunningDown = {}
		return true
	end
	for i = 1,#global.unitsRunningDown,1 do
		if (global.unitsRunningDown[i] == unitID) then
			return false;
		end
	end
	return true
end

local function saveRunning(unitID)
	if (global.unitsRunningDown == nil) then
		global.unitsRunningDown = {}
	end
	global.unitsRunningDown[#global.unitsRunningDown + 1] = unitID
	return true
end

function Run(self, units, parameter)
	local who = parameter.unitID
	
	if (who == nil) then
		return FAILURE
	end
	
	local startX = 4000
	local startZ = 4000
	local goalX = 4000
	local goalZ = 7700
	
	local tolerance = 77
		
	local cx, cy, cz = Spring.GetUnitPosition(who)
	if (cx == nil) or (cz == nil) then
		return FAILURE
	elseif (goalX + tolerance > cx) and (goalX - tolerance < cx) and (goalZ + tolerance > cz) and (goalZ - tolerance < cz) then
		for i = 1,#global.unitsRunningDown,1 do
			if (global.unitsRunningDown[i] == unitID) then
				global.unitsRunningDown[i] = nil
			end
		end
		return SUCCESS
	elseif (global.stackDown ~= nil) then
		if (shouldRun(who)) then
			Spring.GiveOrderToUnit(who, CMD.MOVE, Vec3(startX, 0, startZ):AsSpringVector(), {})
			for i=#global.stackDown,1,-2 do
				Spring.GiveOrderToUnit(who, CMD.MOVE, global.stackDown[i]:AsSpringVector(), {"shift"})
			end
			Spring.GiveOrderToUnit(who, CMD.MOVE, Vec3(goalX, 0, goalZ):AsSpringVector(), {"shift"})
			saveRunning(who)
		end
		return RUNNING
	else
		if (shouldRun(who)) then
			
			local safeHeight = 50

			Spring.GiveOrderToUnit(who, CMD.MOVE, Vec3(startX, 0, startZ):AsSpringVector(), {})
		
			local step = 50
			water = {}
			index = {}
			succ = {}
			for i = 0,8000,step do
				index[i] = {}
				for j = 0,8000,step do
					index[i][j] = 0
					h = Spring.GetGroundHeight(i, j)
					if (h < safeHeight) then
						water[#water + 1] = Vec3(i, h, j)
						succ[#water] = {}
						index[i][j] = #water
						if (i > 0) then
							if (Spring.GetGroundHeight(i-step, j) < safeHeight) then
								succ[#water][#succ[#water]+1] = index[i-step][j]
								succ[index[i-step][j]][#(succ[index[i-step][j]])+1] = #water
							end
						end
						if (j > 0) then
							if (Spring.GetGroundHeight(i, j-step) < safeHeight) then
								succ[#water][#succ[#water]+1] = index[i][j-step]
								succ[index[i][j-step]][#(succ[index[i][j-step]])+1] = #water
							end
						end
						
					end
				end
			end
			
			local queue = {index[startX][startZ]}
			local q_out = 1
			local pred = {}
			while (#queue >= q_out) do
				local current = queue[q_out]
				
				if (water[current].x > goalX - tolerance) and (water[current].z > goalZ - tolerance) 
				and (water[current].x < goalX + tolerance) and (water[current].z < goalZ + tolerance) then
					local node = current;
					local stack = {}
					while (pred[node] ~= nil) do
						stack[#stack+1] = node
						node = pred[node]
					end
					global.stackDown = {}
					for i=1,#stack,1 do
						global.stackDown[i] = water[stack[i]]
					end
					
					for i=#stack,1,-2 do
						Spring.GiveOrderToUnit(who, CMD.MOVE, water[stack[i]]:AsSpringVector(), {"shift"})
					end
					Spring.GiveOrderToUnit(who, CMD.MOVE, Vec3(goalX, 0, goalZ):AsSpringVector(), {"shift"})
					saveRunning(who)
					return RUNNING
				end
				
				for i = 1,#succ[current],1 do
					local seen = 0
					for j = 1,#queue,1 do
						if (queue[j] == succ[current][i]) then
							seen = seen + 1
						end
					end
					if (seen == 0) then
						queue[#queue+1] = succ[current][i]
						pred[succ[current][i]] = current
					end
				end
				q_out = q_out + 1
			end
			
			global.unitsRunningDown[#global.unitsRunningDown + 1] = unitID
		end
		return RUNNING
	end
end


function Reset(self)
	ClearState(self)
end
