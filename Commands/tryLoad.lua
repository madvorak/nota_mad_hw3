function getInfo()
	return {
		onNoUnits = SUCCESS,
		tooltip = "Try to load one unit.",
		parameterDefs = {
			{ 
				name = "atlasID",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
			{ 
				name = "targetID",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			}
		}
	}
end

-- constants
local THRESHOLD_DEFAULT = 0

local function ClearState(self)
	self.threshold = THRESHOLD_DEFAULT
end

function Run(self, units, parameter)
	local who = parameter.atlasID -- unitID
	local what = parameter.targetID -- unitID
	
	-- Spring.Echo(UnitDefs[Spring.GetUnitDefID(who)].humanName)
	-- Spring.Echo(UnitDefs[Spring.GetUnitDefID(what)].humanName)
	
	if (who == nil) or (what == nil) then
		return FAILURE
	end
	
	local transporting = Spring.GetUnitIsTransporting(who)
	
	if (transporting == nil) or (#transporting == 0) then 
		Spring.GiveOrderToUnit(who, CMD.LOAD_UNITS, {what}, {})
		return RUNNING		
	else
		return SUCCESS
	end
end


function Reset(self)
	ClearState(self)
end
