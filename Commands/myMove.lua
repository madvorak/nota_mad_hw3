function getInfo()
	return {
		onNoUnits = SUCCESS,
		tooltip = "Move to defined position",
		parameterDefs = {
			{ 
				name = "unitID",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
			{ 
				name = "x",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
			{ 
				name = "z",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
		}
	}
end

-- constants
local THRESHOLD_DEFAULT = 0

local function ClearState(self)
	self.threshold = THRESHOLD_DEFAULT
end

function Run(self, units, parameter)
	local who = parameter.unitID
	local x = parameter.x
	local z = parameter.z
	
	if (who == nil) then
		return FAILURE
	end
		
	local cx, cy, cz = Spring.GetUnitPosition(who)
	if (cx == nil) or (cz == nil) then
		return FAILURE
	elseif (x + 10 > cx) and (x - 10 < cx) and (z + 10 > cz) and (z - 10 < cz) then
		return SUCCESS
	else 
		Spring.GiveOrderToUnit(who, CMD.MOVE, Vec3(x, 0, z):AsSpringVector(), {})
		return RUNNING
	end
end


function Reset(self)
	ClearState(self)
end
